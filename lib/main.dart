import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_display_machinetest/provider/weather_provider.dart';
import 'package:weather_display_machinetest/views/screens/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => WeatherProvider()),
      ],
      child: MaterialApp(
        title: 'weather',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
        ),
        home: const SplashScreen(),
      ),
    );
  }
}
