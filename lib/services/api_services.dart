import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../domain/core/app_utils.dart';
import '../provider/weather_provider.dart';
import 'package:http/http.dart' as http;

class ApiService {
  Future getAllData(
    context,
  ) async {
    isConnected(context).then(
      (value) async {
        if (value) {
          try {
            String ip = "";
            String city = "";

            final state = Provider.of<WeatherProvider>(context, listen: false);
            final response3 =
                await http.get(Uri.parse("https://api.ipify.org/?format=json"));
            if (response3.statusCode == 200) {
              var data = json.decode(response3.body);
              ip = data["ip"];

              final respone2 =
                  await http.get(Uri.parse("https://ipinfo.io/$ip/geo"));
              if (respone2.statusCode == 200) {
                var data2 = json.decode(respone2.body);

                city = data2["city"];
              }
              final response = await http.get(Uri.parse(
                  "http://api.weatherapi.com/v1/current.json?key=c0dbb6f1794640eeabf103014222805&q=$city&aqi=no"));

              if (response.statusCode == 200) {
                state.loading = false;

                state.addWeather(jsonEncode(response.body));
              } else {
                showMessage("Something went wrong!,Please try again",
                    Colors.redAccent, context);
              }
            } else {
              showMessage("Something went wrong!,Please try again",
                  Colors.redAccent, context);
            }
          } catch (e) {
            showMessage(e.toString(), Colors.redAccent, context);
          }
        } else {
          showMessage("Oops! Check your internet connection", Colors.redAccent,
              context);
        }
      },
    );
  }
}
