import 'dart:convert';

WeatherModel weatherFromJson(String str) =>
    WeatherModel.fromJson(json.decode(str));

class WeatherModel {
  WeatherModel({
  required  this.location,
   required this.current,
  });

 Location location;
  Current current;

  factory WeatherModel.fromJson(Map<String, dynamic> json) => WeatherModel(
        location: json["location"] == null
            ? Location()
            : Location.fromJson(json["location"]),
        current: json["current"] == null
            ? Current()
            : Current.fromJson(json["current"]),
      );
}

class Current {
  Current({
    this.lastUpdatedEpoch,
    this.lastUpdated,
    this.tempC,
    this.tempF,
    this.isDay,
    this.condition,
    this.windMph,
    this.windKph,
    this.windDegree,
    this.windDir,
    this.pressureMb,
    this.pressureIn,
    this.precipMm,
    this.precipIn,
    this.humidity,
    this.cloud,
    this.feelslikeC,
    this.feelslikeF,
    this.visKm,
    this.visMiles,
    this.uv,
    this.gustMph,
    this.gustKph,
  });

  int? lastUpdatedEpoch;
  String? lastUpdated;
  double? tempC;
  double? tempF;
  int? isDay;
  Condition? condition;
  double? windMph;
  double? windKph;
  int? windDegree;
  String? windDir;
  double? pressureMb;
  double? pressureIn;
  double? precipMm;
  double? precipIn;
  int? humidity;
  int? cloud;
  double? feelslikeC;
  double? feelslikeF;
  double? visKm;
  double? visMiles;
  double? uv;
  double? gustMph;
  double? gustKph;

  factory Current.fromJson(Map<String, dynamic> json) => Current(
        lastUpdatedEpoch: json["last_updated_epoch"] ?? 0,
        lastUpdated: json["last_updated"] ?? "",
        tempC: json["temp_c"] ?? 0.0,
        tempF: json["temp_f"] ?? 0.0,
        isDay: json["is_day"] ?? 0,
        condition: Condition.fromJson(json["condition"]),
        windMph: json["wind_mph"] ?? 0.0,
        windKph: json["wind_kph"] ?? 0.0,
        windDegree: json["wind_degree"] ?? 0,
        windDir: json["wind_dir"] ?? "",
        pressureMb: json["pressure_mb"] ?? 0.0,
        pressureIn: json["pressure_in"] ?? 0.0,
        precipMm: json["precip_mm"] ?? 0.0,
        precipIn: json["precip_in"] ?? 0.0,
        humidity: json["humidity"] ?? 0,
        cloud: json["cloud"] ?? 0,
        feelslikeC: json["feelslike_c"] ?? 0.0,
        feelslikeF: json["feelslike_f"] ?? 0.0,
        visKm: json["vis_km"] ?? 0.0,
        visMiles: json["vis_miles"] ?? 0.0,
        uv: json["uv"] ?? 0.0,
        gustMph: json["gust_mph"] ?? 0.0,
        gustKph: json["gust_kph"] ?? 0.0,
      );
}

class Condition {
  Condition({
    this.text,
    this.icon,
    this.code,
  });

  String? text;
  String? icon;
  int? code;

  factory Condition.fromJson(Map<String, dynamic> json) => Condition(
        text: json["text"] ?? "",
        icon: json["icon"] ?? "",
        code: json["code"] ?? 0,
      );
}

class Location {
  Location({
    this.name,
    this.region,
    this.country,
    this.lat,
    this.lon,
    this.tzId,
    this.localtimeEpoch,
    this.localtime,
  });

  String? name;
  String? region;
  String? country;
  double? lat;
  double? lon;
  String? tzId;
  int? localtimeEpoch;
  String? localtime;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"] ?? "",
        region: json["region"] ?? "",
        country: json["country"] ?? "",
        lat: json["lat"] ?? 0.0,
        lon: json["lon"] ?? 0.0,
        tzId: json["tz_id"] ?? "",
        localtimeEpoch: json["localtime_epoch"] ?? 0,
        localtime: json["localtime"] ?? "",
      );
}
