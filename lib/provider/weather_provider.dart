import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:weather_display_machinetest/services/response/weather_model.dart';

class WeatherProvider with ChangeNotifier {
  late WeatherModel weatherData;

  bool loading = false;

  addWeather(String response) async {
    final decode = jsonDecode(response);
    if (decode != null) {
      weatherData = WeatherModel.fromJson(json.decode(decode));

      notifyListeners();
    }
  }
}
