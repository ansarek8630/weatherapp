import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:weather_display_machinetest/views/screens/dashboard_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    checkConnection();
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.purple,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Center(
              child: Text(
                "Lilac Flutter Assignment",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w800),
                //style: StyleResources.secondaryButton(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      Future.delayed(const Duration(seconds: 3)).then((value) => Navigator.push(
          context,
          MaterialPageRoute(builder: (builder) => const DashboardScreen())));
      // I am connected to a mobile network.
    } else if (connectivityResult == ConnectivityResult.wifi) {
      Future.delayed(const Duration(seconds: 3)).then((value) => Navigator.push(
          context,
          MaterialPageRoute(builder: (builder) => const DashboardScreen())));
      // I am connected to a wifi network.
    } else {
      // ignore: use_build_context_synchronously
      showAlertDialog(context);
    }
  }

  showAlertDialog(BuildContext context) {
    Dialog errorDialog = Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: SizedBox(
        height: 300.0,
        width: 300.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                'No internet connection..!',
                style: TextStyle(color: Colors.red),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15.0),
              child: Text(
                'check your internet connection',
                style: TextStyle(color: Colors.red),
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 50.0)),
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(
                  'close',
                  style: TextStyle(color: Colors.purple, fontSize: 18.0),
                ))
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => errorDialog);
  }
}
