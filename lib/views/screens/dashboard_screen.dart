import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_display_machinetest/provider/weather_provider.dart';
import 'package:weather_display_machinetest/services/api_services.dart';
import 'package:weather_display_machinetest/views/components/weather_widget.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  void initState() {
    super.initState();

    getData();
  }

  getData() async {
    final state = Provider.of<WeatherProvider>(context, listen: false);
    state.loading = true;
    
    await ApiService().getAllData(context);
     state.loading = false;
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: Container(
          height: size.height,
          width: size.width,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/sunny.png"),
                  fit: BoxFit.fill)),
          child: Consumer<WeatherProvider>(builder: (context, model, _) {
            if (model.loading) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 158.0, vertical: 300),
                child: CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 5,
                ),
              );

              // ignore: unnecessary_null_comparison
            } else if (model.loading == false && model.weatherData != null) {
              return Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 16.0, vertical: 16.0),
                child: WeatherWidget(
                  model: model.weatherData,
                ),
              );
            } else {
              return const Center(child: Text("no data"));
            }
          }),
        ),
      ),
    );
  }
}
