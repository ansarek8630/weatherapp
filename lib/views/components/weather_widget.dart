import 'package:flutter/material.dart';
import 'package:weather_display_machinetest/services/response/weather_model.dart';

class WeatherWidget extends StatelessWidget {
  final WeatherModel model;
  const WeatherWidget({super.key, required this.model});

  @override
  Widget build(BuildContext context) {
    const height10 = SizedBox(
      height: 10,
    );
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            children: [
              Text(
                model.location.name!,
                style:
                    const TextStyle(fontSize: 35, fontWeight: FontWeight.w300),
              ),
              Text(
                "${model.location.region!},${model.location.country!}",
                textAlign: TextAlign.start,
                style: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w300,
                ),
              ),
              //  Text(
              //   model.location.localtime!,
              //   style: const TextStyle(
              //     fontSize: 15,
              //     fontWeight: FontWeight.w300,
              //   ),
              // ),
            ],
          ),
        ),
        Text(
          "${model.current.tempC.toString()}°C",
          style: const TextStyle(fontSize: 55, fontWeight: FontWeight.w400),
        ),
        Row(
          children: [
            Image.network(
              'https:${model.current.condition!.icon!}',
            ),
            Text(
              model.current.condition!.text!,
            )
          ],
        ),
        const Divider(
          thickness: 2,
        ),
        height10,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                const Text("Wind"),
                Text(model.current.windKph.toString()),
                const Text("km/h"),
              ],
            ),
            Column(
              children: [
                const Text("UV"),
                Text(model.current.uv.toString()),
                const Text("%"),
              ],
            ),
            Column(
              children: [
                const Text("Humidity"),
                Text(model.current.humidity.toString()),
                const Text("%"),
              ],
            )
          ],
        ),
        height10,
      ],
    );
  }
}
