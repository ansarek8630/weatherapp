import 'dart:io';
import 'package:flutter/material.dart';

Widget appLoading({double? value}) {
  return CircularProgressIndicator(
    value: value,
    color: Colors.grey,
    backgroundColor: Colors.redAccent,
  );
}

Future<bool> isConnected(BuildContext context) async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
  } on SocketException catch (e) {
    return false;
  }
  return false;
}

void showMessage(String message, Color colorBackground, context) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      duration: const Duration(seconds: 5),
      backgroundColor: colorBackground,
      content: Text(
        message,
        style: const TextStyle(letterSpacing: 0.8),
      ),
    ),
  );
}
